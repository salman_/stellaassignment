import { AssignmentStellaPage } from './app.po';

describe('assignment-stella App', function() {
  let page: AssignmentStellaPage;

  beforeEach(() => {
    page = new AssignmentStellaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
