import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from 'app/_services';
import { User } from 'app/_models';

@Component({
  selector: 'app-qa',
  templateUrl: './qa.component.html',
  styleUrls: ['./qa.component.css']
})
export class QaComponent implements OnInit {

  users: User[] = [];
  public isLoggedin = false;
  constructor(private userService: UserService , private authservice : AuthenticationService) {
      this.isLoggedin = this.authservice.isLoggedIn;
  }

  ngOnInit() {
    this.loadAllQa()
  }
  deleteUser(id: number) {
    this.userService.delete(id).subscribe(() => { this.loadAllQa() });
  }
  create(n) {
      this.userService.addNewQa({name:n}).subscribe(() => { this.loadAllQa() });
  }

  private loadAllQa() {
      this.userService.getAllQa().subscribe(users => { this.users = users; });
  }

}
