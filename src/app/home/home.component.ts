import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserService , AuthenticationService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {

    users: User[] = [];
    public isLoggedin = false;
    constructor(private userService: UserService , private authservice : AuthenticationService) {
        this.isLoggedin = this.authservice.isLoggedIn;
    }

    ngOnInit() {
        this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }
    create(n,d) {
        this.userService.addNew({name:n,designation:d}).subscribe(() => { this.loadAllUsers() });
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(users => { this.users = users; });
    }
}