import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { AppComponent } from './app.component';
import { UserService, AlertService, AuthenticationService } from 'app/_services';
import { AuthGuard } from 'app/_guards';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AlertComponent } from './_directives/index';
import { routing } from 'app/app.routing';
import { DeveloperComponent } from './developer/developer.component';
import { DesignerComponent } from './designer/designer.component';
import { HrComponent } from './hr/hr.component';
import { QaComponent } from './qa/qa.component';
import { NavComponent } from './nav/nav.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    DeveloperComponent,
    DesignerComponent,
    HrComponent,
    QaComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    NgIdleKeepaliveModule.forRoot()
  ],
  providers: [AuthGuard,AlertService,AuthenticationService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
