import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private http: Http) { }
    private _contactUrl = 'http://localhost/stellaci/login';
    public isLoggedIn = false;
    
    login(value:any) 
    {
        const body = new URLSearchParams(value);
        body.set('username', value.data[0].username);
        body.set('password', value.data[0].password);
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(this._contactUrl, body, {
          headers: headers
        }).map((response: Response) => {
                let flag = response.json();
                if (flag === true) 
                {
                    this.isLoggedIn = true;
                }
            });
    }

    logout() 
    {
        this.isLoggedIn = false;
    }
    loginFlag() 
    {
        return this.isLoggedIn;
    }
}