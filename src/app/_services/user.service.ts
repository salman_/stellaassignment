import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../_models/index';

@Injectable()
export class UserService {
    constructor(private http: Http) { }
    private _contactUrl = 'http://localhost/stellaci/';
    
    getAll() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.get(this._contactUrl+'getall').map((response: Response) => response.json());
    }
    getAllDevs() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.get(this._contactUrl+'getalldevs').map((response: Response) => response.json());
    }
    getAllDesg() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.get(this._contactUrl+'getalldesg').map((response: Response) => response.json());
    }
    getAllHr() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.get(this._contactUrl+'getallhr').map((response: Response) => response.json());
    }
    getAllQa() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.get(this._contactUrl+'getallqa').map((response: Response) => response.json());
    }
    create(user: User) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        return this.http.post(this._contactUrl+'register', user,{headers: headers}).map((response: Response) => response.json());
    }
    addNew(obj) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        return this.http.post(this._contactUrl+'addnew', obj ,{headers: headers}).map((response: Response) => response.json());
    }
    addNewDev(obj) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        return this.http.post(this._contactUrl+'addnewdev', obj ,{headers: headers}).map((response: Response) => response.json());
    }
    addNewDesg(obj) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        return this.http.post(this._contactUrl+'addnewdesg', obj ,{headers: headers}).map((response: Response) => response.json());
    }
    addNewHr(obj) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        return this.http.post(this._contactUrl+'addnewhr', obj ,{headers: headers}).map((response: Response) => response.json());
    }
    addNewQa(obj) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        return this.http.post(this._contactUrl+'addnewqa', obj ,{headers: headers}).map((response: Response) => response.json());
    }
    delete(id: number) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.delete(this._contactUrl+'delete/' + id, {headers: headers}).map((response: Response) => response.json());
    }
    
}