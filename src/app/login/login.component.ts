import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService } from '../_services/index';
import { User } from '../_models';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
})

export class LoginComponent implements OnInit {
    //model: any = {};
    //loading = false;
    returnUrl: string;
    usernam: string = "";
    passwor: string = "";

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        //this.loading = true;
        let user = new User('', '');
        user.username = this.usernam;
        user.password = this.passwor;
        let data = [
          { 'username': user.username, 'password': user.password }
        ];

        this.authenticationService.login({ data })
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                    if(!this.authenticationService.isLoggedIn)
                    {
                        this.alertService.error('Username or Password incorrect!', false);
                    }
                },
                error => {
                    this.alertService.error(error);
                    //this.loading = false;
                });
    }
}
