import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AuthGuard } from './_guards/index';
import { DeveloperComponent } from 'app/developer/developer.component';
import { DesignerComponent } from 'app/designer/designer.component';
import { QaComponent } from 'app/qa/qa.component';
import { HrComponent } from 'app/hr/hr.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'developer', component: DeveloperComponent ,canActivate: [AuthGuard] },
    { path: 'designer', component: DesignerComponent,canActivate: [AuthGuard] },
    { path: 'hr', component: HrComponent,canActivate: [AuthGuard] },
    { path: 'qa', component: QaComponent,canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);