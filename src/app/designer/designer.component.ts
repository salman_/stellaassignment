import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from 'app/_services';
import { User } from 'app/_models';

@Component({
  selector: 'app-designer',
  templateUrl: './designer.component.html',
  styleUrls: ['./designer.component.css']
})
export class DesignerComponent implements OnInit {

  users: User[] = [];
  public isLoggedin = false;
  constructor(private userService: UserService , private authservice : AuthenticationService) {
      this.isLoggedin = this.authservice.isLoggedIn;
  }
  ngOnInit() {
    this.loadAllDesigners()
  }
  deleteUser(id: number) {
    this.userService.delete(id).subscribe(() => { this.loadAllDesigners() });
  }
  create(n) {
      this.userService.addNewDesg({name:n}).subscribe(() => { this.loadAllDesigners() });
  }

  private loadAllDesigners() {
      this.userService.getAllDesg().subscribe(users => { this.users = users; });
  }
}
