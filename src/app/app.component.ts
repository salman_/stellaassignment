import { Component } from '@angular/core';
import { AuthenticationService } from 'app/_services';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public isLoggedin = false;
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  constructor(private router: Router,private authservice : AuthenticationService,private idle: Idle, private keepalive: Keepalive) {
      this.isLoggedin = this.authservice.isLoggedIn;
      idle.setIdle(50);
      idle.setTimeout(50);
      idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
      idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
      idle.onTimeout.subscribe(() => {
        this.idleState = 'Timed out!';
        this.timedOut = true;
        this.router.navigate(['/login']);
      });
      idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
      idle.onTimeoutWarning.subscribe((countdown) => this.idleState = 'You will time out in ' + countdown + ' seconds!');
  
      // sets the ping interval to 60 seconds
      keepalive.interval(25);
  
      keepalive.onPing.subscribe(() => this.lastPing = new Date());
  
      this.reset();
  }
  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
}
