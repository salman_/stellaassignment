import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from 'app/_services';
import { User } from 'app/_models';

@Component({
  selector: 'app-hr',
  templateUrl: './hr.component.html',
  styleUrls: ['./hr.component.css']
})
export class HrComponent implements OnInit {

  users: User[] = [];
  public isLoggedin = false;
  constructor(private userService: UserService , private authservice : AuthenticationService) {
      this.isLoggedin = this.authservice.isLoggedIn;
  }

  ngOnInit() {
    this.loadAllHr()
  }
  deleteUser(id: number) {
    this.userService.delete(id).subscribe(() => { this.loadAllHr() });
  }
  create(n) {
      this.userService.addNewHr({name:n}).subscribe(() => { this.loadAllHr() });
  }

  private loadAllHr() {
      this.userService.getAllHr().subscribe(users => { this.users = users; });
  }

}
