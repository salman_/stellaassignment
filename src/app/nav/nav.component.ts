import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from 'app/_services';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public isLoggedin = false;
  constructor(private userService: UserService , private authservice : AuthenticationService) {
      this.isLoggedin = this.authservice.isLoggedIn;
  }

  ngOnInit() {
  }

}
