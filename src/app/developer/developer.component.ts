import { Component, OnInit } from '@angular/core';
import { UserService , AuthenticationService } from '../_services/index';
import { User } from '../_models/index';

@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.css']
})
export class DeveloperComponent implements OnInit {
  users: User[] = [];
  public isLoggedin = false;
  constructor(private userService: UserService , private authservice : AuthenticationService) {
      this.isLoggedin = this.authservice.isLoggedIn;
  }

  ngOnInit() {
    this.loadAllDevelopers();
  }
  deleteUser(id: number) {
    this.userService.delete(id).subscribe(() => { this.loadAllDevelopers() });
  }
  create(n) {
      this.userService.addNewDev({name:n}).subscribe(() => { this.loadAllDevelopers() });
  }

  private loadAllDevelopers() {
      this.userService.getAllDevs().subscribe(users => { this.users = users; });
  }
}
